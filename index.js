var array = [];
var total = 0;
var positive = 0;
var smallest;
var positiveArray = [];
var smallestPositive;
var even;

function themSo() {
  var number = +document.getElementById("number").value;
  array.push(number);
  console.log(array);
  smallest = array[0];

  if (number > 0) {
    total += number;
    positive++;
    positiveArray.push(number);
  }

  document.getElementById("notification").innerHTML = `Mảng hiện tại: ${array}`;
}

function tongSoDuong() {
  document.getElementById(
    "notification1"
  ).innerHTML = `Tổng số dương: ${total}`;
}

function demSoDuong() {
  document.getElementById("notification2").innerHTML = `Số dương: ${positive}`;
}

function soNhoNhat() {
  for (var i = 1; i < array.length; i++) {
    var soHienTai = array[i];
    smallest = array[0];
    if (soHienTai < smallest) {
      smallest = soHienTai;
    }
  }
  console.log(smallest);

  //   document.getElementById(
  //     "notification3"
  //   ).innerHTML = `Số nhỏ nhất: ${smallest}`;
}

function soDuongNhoNhat() {
  if (positiveArray.length == 0) {
    document.getElementById("notification4").innerHTML = `Không có số dương`;
  } else {
    for (let i = 0; i < positiveArray.length; i++) {
      var soHienTai = positiveArray[i];
      smallestPositive = array[0];
      if (soHienTai < smallestPositive) {
        smallestPositive = soHienTai;
      }
    }
    document.getElementById(
      "notification5"
    ).innerHTML = `Số dương nhỏ nhất: ${smallestPositive}`;
  }
}

function soChanCuoiCung() {
  for (var i = 0; i < array.length; i++) {
    var soHienTai = array[i];
    if (soHienTai % 2 == 0) {
      even = soHienTai;
    }
  }
  document.getElementById(
    "notification6"
  ).innerHTML = `Số chẵn cuối cùng là: ${even}`;
}

function doiSo() {
  var first = +document.getElementById("first").value;
  var second = +document.getElementById("second").value;

  var trungGian = array[first];
  array[first] = array[second];
  array[second] = trungGian;

  document.getElementById(
    "notification7"
  ).innerHTML = `Mảng sau khi đổi: ${array}`;
}

function sapXepTangDan() {
  // Buble sort
  for (var i = 0; i < array.length; i++) {
    // Last i elements are already in place
    for (var j = 0; j < array.length - i - 1; j++) {
      // Checking if the item at present iteration
      // is greater than the next iteration
      if (array[j] > array[j + 1]) {
        // If the condition is true then swap them
        var temp = array[j];
        array[j] = array[j + 1];
        array[j + 1] = temp;
      }
    }
  }

  document.getElementById(
    "notification6"
  ).innerHTML = `Mảng sau khi sắp xếp: ${array}`;
}

function checkPrime(number) {
  if (number == 1) {
    return false;
  }
  else if (number > 1) {
    for (let i = 2; i < number; i++) {
      if (number % i == 0) {
        return false;
      }
    }
    return true;
  }
}

function timSoNguyenTo() {
  for (var i = 0; i < array.length; i++) {
    if (checkPrime(array[i]) == true) {
      document.getElementById(
        "notification9"
      ).innerHTML = `Số nguyên tố đầu tiên: ${array[i]}`;
      break;
    } else {
      document.getElementById("notification9").innerHTML = `-1`;
    }
  }
}

function demSoNguyen() {
  var number = +document.getElementById("numberPlus").value;
  array.push(number);
  var integer = 0;
  for (var i = 0; i < array.length; i++) {
    if (Number.isInteger(array[i]) == true) {
      integer++;
    }
  }
  document.getElementById("notification10").innerHTML = `Số nguyên: ${integer}`
}

function soSanh() {
  var positive = 0;
  var negative = 0;
  for (var i = 0; i < array.length; i++) {
    if (array[i] > 0) {
      positive++;
    } else {
      negative++;
    }
  }

  if (positive > negative) {
    document.getElementById("notification11").innerHTML = `Số dương > Số âm`
  } else if (positive < negative) {
    document.getElementById("notification11").innerHTML = `Số dương < Số âm`
  } else {
    document.getElementById("notification11").innerHTML = `Số dương = Số âm`
  }
}


